module.exports = {
  plugins: [],
  theme: {
    fontSize: {
    //  'xs': '.75rem',
     'sm': '12px',
    //  'tiny': '.875rem',
    //   'base': '1rem',
      'lg': '20px',
    //   'xl': '1.25rem',
    //   '2xl': '1.5rem',
    //  '3xl': '1.875rem',
    //  '4xl': '2.25rem',
    //   '5xl': '3rem',
    //   '6xl': '4rem',
    //  '7xl': '5rem',
    },
    borderRadius: {
      'sm':'10px',
      DEFAULT: '10px'
    }
  }
}