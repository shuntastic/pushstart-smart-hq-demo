import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    modalActive:false,
    timezoneOffset:0,
    timezone:'',
    path: 'home',
    authURL:'http://165.22.4.29/auth/local',
    api: 'http://165.22.4.29/',
    /*
    process.env.BASE_URL.indexOf('localhost') > -1
    http://165.22.4.29/
        ? 'http://localhost:1337/'
        : process.env.BASE_URL.indexOf('shun.io') > -1
        ? 'http://gpp.shun.io/engine/'
        : 'http://globalpeacepractice.org/engine/',
    */

    user: {
      authenticated: false,
      usernname: '',
      firstname: '',
      lastname: '',
      timezone: '',
      token:''
    },
  },

  getters: {
    userInfo: (state) => (target) => {
      return state.user[target];
    },
  },

  mutations: {
    setUserStatus(state, payload) {
      state.user[payload.target] = payload.status;
    },
    setState(state, payload) {
      state[payload.target] = payload.value;
    },
  },

  actions: {
    setUserStatus(context, payload) {
      context.commit('setUserStatus', payload);
    },
    setState(context, payload) {
      context.commit('setState', payload);
    },
  },
});
